#include "libstop.h"
#include <stdio.h>

int main()
/* Programa para dar el resultado del cubo de un numero*/ 
{
    printf ("Programa del cubo de un número\n");
int num, res;
/* Solicitando numero a ingresar*/
    printf("Ingrese el numero para elevarlo al cubo:\t");
    scanf("%d",&num);

    if (num<0){
         printf("Error ingrese un valor correcto");
    }else{        
        res=cubo(num);
        /* Mostrando resultado*/
      printf("El resultado de elevar %d al cubo es: %d\t\n",num,res);
 }

    return 0;
}


long cubo (int num){
    if(num==0 || num==1){
        return 1;
    }else if(num>1){
        return (num*num*num);
    }
}
